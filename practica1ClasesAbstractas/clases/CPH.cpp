/*
 * MMDP.cpp
 *
 *  Created on: 06/02/2013
 *      Author: pedroa
 */

#include "CPH.h"
#include "SolucionCPH.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdlib>  // Para establecer la semilla srand() y generar números aleatorios rand()
#include <limits>
#include <math.h>
#include <float.h>

using namespace std;

namespace clases {

CPH::CPH() {
	data.n=-1;
	data.p=-1;
	data.c=-1;
}

CPH::~CPH() {
	if(data.d!=0 || data.f!=0)
		liberarMemoria();
}

int CPH::reservarMemoria() {
	if(data.n!=-1){
		data.d = new float*[data.n];
		if(data.n==0)
			return -3;
		for(int i=0; i<data.n; i++){
			data.d[i] = new float[data.n];
			if(data.d[i]==0)
				return -3;
		}

		for(int i=0; i<data.n; i++)
			for(int j=0; j<data.n; j++)
				data.d[i][j] = 0.;

		data.f = new float[data.n];
		if(data.f==0)
			return -3;

		return 1;
	}
	else
		return -3;
}

void CPH::liberarMemoria() {
	if(data.d!=0){
		for(int i=0; i<data.n; i++)
			delete[] data.d[i];
		delete[] data.d;
	}
	delete[] data.f;
}

Solucion * CPH::reservarMemoriaSolucion(){
	SolucionCPH *sol = new SolucionCPH;
	sol->x = new int[data.n];
	for(int i=0; i<data.n; i++){
		sol->x[i] = -1;
	}
	return sol;

}

void CPH::liberarMemoriaSolucion(Solucion *solucion){
	SolucionCPH *sol = dynamic_cast<SolucionCPH*>(solucion);
	delete[] sol->x;
	delete sol;
}

int CPH::leerInstancia(char *archivo) {
	ifstream myFile(archivo);    // Crear un stream de entrada

    if (!myFile.is_open()) {
       cout << "ERROR: no puedo abrir el fichero " << archivo << endl;
       return -1;
    }
	string linea;
	if( myFile.good()) {
		getline(myFile,linea);   // Leer una línea
		if (! linea.empty()) {
			istringstream iss(linea);
			iss >> data.n;
			if(!iss)
				return -2;
			iss >> data.p;
			if(!iss)
				return -2;
			iss >> data.c;
			if(!iss)
				return -2;

		}
	}

	if(reservarMemoria()==-3)
		return -3;


	float ** coordenadas = new float*[data.n];
	if(coordenadas == 0)
		return -3;
	for(int i=0; i<data.n; i++){
		coordenadas[i] = new float[2];
		if(coordenadas[i]==0)
			return -3;
	}

	while ( myFile.good()) {
		getline(myFile,linea);   // Leer una línea
		if(!linea.empty()){
			istringstream iss(linea);
			int iIndex;
			float value;
			iss >> iIndex;
			if(!iss)
				return -2;

			iss >> value;
			if(!iss)
				return -2;
			coordenadas[iIndex-1][0] = value;

			iss >> value;
			if(!iss)
				return -2;
			coordenadas[iIndex-1][1] = value;

			iss >> value;
			data.f[iIndex-1] = value;

			if(!iss)
				return -2;
		}
	}


	for(int i=0; i<data.n; i++){
		for(int j=0; j<data.n; j++){
			float sum = 0;
			for(int k=0; k<2; k++){
				float diff = coordenadas[i][k] - coordenadas[j][k];
				sum += diff*diff;
			}
			data.d[i][j] = sqrt(sum);
		}

	}

	for(int i=0; i<data.n; i++)
		delete[] coordenadas[i];
	delete[] coordenadas;

	myFile.close();


	return 1;
}


float CPH::evaluarSolucion(Solucion *solucion) {
	SolucionCPH *sol = dynamic_cast<SolucionCPH*>(solucion);
	float distanciaTotal=0;
	// Cálculo de la distancia y comprobación de carga
	for(int i=0; i<data.p; i++){
		float carga = 0;
		int nodoI = -1;
		for(int j=0; j<data.n; j++)
			if(sol->x[j]==(data.p + i)){
				nodoI = j;
				carga += data.f[j];
				j = data.n;
			}
		if(nodoI==-1){
			cout << "ERROR" << endl;
			exit(1);
		}
		for(int j=0; j<data.n; j++){
			if(sol->x[j]==i){
				distanciaTotal += data.d[nodoI][j];
				carga += data.f[j];
			}
		}
		if(carga > data.c)
			return FLT_MAX;
	}
	return distanciaTotal;
}

Solucion * CPH::generarSolucionRandom() {
	SolucionCPH *sol = NULL;
	if(data.n!=-1){
		sol = dynamic_cast<SolucionCPH*>(reservarMemoriaSolucion());

		// Elegir p nodos como concentradores
		int* numerosPorElegir = new int[data.n];
		int* numerosElegidos = new int[data.p];
		// Inicializar la lista de elecciones posibles
		for(int i = 0; i < data.n; i++)
			numerosPorElegir[i] = i;

		for(int i=0; i < data.p; i++)
		{
			int numeroElegido = rand() % (data.n-i);
			// Recordar el numero elegido
			numerosElegidos[i]=numerosPorElegir[numeroElegido];
			// Ponemos en numeros por elegir, el ultimo que era valido, asi
			// son todos validos hasta data.n-i-1
			numerosPorElegir[numeroElegido]=numerosPorElegir[data.n-i-1];
		}

		for(int i=0; i<data.p; i++)
			sol->x[numerosElegidos[i]] = data.p+i;

		delete [] numerosElegidos;
		numerosElegidos = new int[data.n];

		// Inicializar la lista de elecciones posibles
		for(int i = 0; i < data.n; i++)
			numerosPorElegir[i] = i;

		for(int i=0; i < data.n; i++)
		{
			int numeroElegido = rand() % (data.n-i);
			// Recordar el numero elegido
			numerosElegidos[i]=numerosPorElegir[numeroElegido];
			// Ponemos en numeros por elegir, el ultimo que era valido, asi
			// son todos validos hasta data.n-i-1
			numerosPorElegir[numeroElegido]=numerosPorElegir[data.n-i-1];
		}

		// Conectar cada cliente a un concentrador, de forma aleatoria
		for(int i = 0; i < data.n; i++){
			if(sol->x[numerosElegidos[i]]==-1){
				int concentradorElegido = 0;
				float menorCarga = 0;
				for(int j=0; j<data.p; j++){
					float carga = 0;
					for(int k=0; k<data.n; k++){
						if(sol->x[k]==j || (sol->x[k]==(data.p+j))) //Cliente conectado a ese hub o el propio hub
							carga+=data.f[k];
					}
					if(j==0 || carga<menorCarga){
						concentradorElegido=j;
						menorCarga=carga;
					}
				}
//				int j;
//				for(j=0; j<data.n && concentradorElegido!=-1;j++){
//					if(sol->y[j])
//						concentradorElegido--;
//				}
				sol->x[numerosElegidos[i]]=concentradorElegido;
			}
		}

		delete [] numerosPorElegir;
		delete [] numerosElegidos;
	}

	return sol;
}

void CPH::imprimirSolucion(Solucion *solucion) {
	SolucionCPH *sol =dynamic_cast<SolucionCPH*>(solucion);
	cout << "***************************" << endl;
	cout << "Concentradores: " << endl;
	for(int i=0; i<data.n; i++){
		cout << sol->x[i] << " ";
	}
	cout << "***************************" << endl;

}

void CPH::imprimirInstancia() {
	cout << "===========================" << endl;
	cout << "Dimensiones:" << endl;
	cout << "n=" << data.n << ", p=" << data.p << ", c=" << data.c << endl;
	cout << "Matriz de distancias:" << endl;
	for(int i=0; i<data.n; i++){
		for(int j=0; j<data.n; j++){
			cout << data.d[i][j] << " ";
		}
		cout << endl;
	}
	cout << "===========================" << endl;
}

}
