/*
 * MMDP.cpp
 *
 *  Created on: 06/02/2013
 *      Author: pedroa
 */

#include "TSP.h"
#include "SolucionTSP.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdlib>  // Para establecer la semilla srand() y generar números aleatorios rand()
#include <limits>
#include <math.h>
#include <float.h>

using namespace std;

namespace clases {

TSP::TSP() {
	data.m=-1;
}

TSP::~TSP() {
	if(data.d!=0)
		liberarMemoria();
}

int TSP::reservarMemoria() {
	if(data.m!=-1){
		data.d = new float*[data.m];
		if(data.m==0)
			return -3;
		for(int i=0; i<data.m; i++){
			data.d[i] = new float[data.m];
			if(data.d[i]==0)
				return -3;
		}

		for(int i=0; i<data.m; i++)
			for(int j=0; j<data.m; j++)
				data.d[i][j] = 0.;

		return 1;
	}
	else
		return -3;
}

void TSP::liberarMemoria() {
	if(data.d!=0){
		for(int i=0; i<data.m; i++)
			delete[] data.d[i];
		delete[] data.d;
	}
}

Solucion * TSP::reservarMemoriaSolucion(){
	SolucionTSP *sol = new SolucionTSP;
	sol->x = new int[data.m];
	for(int i=0; i<data.m; i++){
		sol->x[i] = -1;
	}
	return sol;

}

void TSP::liberarMemoriaSolucion(Solucion *solucion){
	SolucionTSP *sol =dynamic_cast<SolucionTSP*>(solucion);
	delete[] sol->x;
	delete sol;
}

int TSP::leerInstancia(char *archivo) {
	ifstream myFile(archivo);    // Crear un stream de entrada

    if (!myFile.is_open()) {
       cout << "ERROR: no puedo abrir el fichero " << archivo << endl;
       return -1;
    }
	string linea;
	if( myFile.good()) {
		getline(myFile,linea);   // Leer una línea
		if (! linea.empty()) {
			istringstream iss(linea);
			iss >> data.m;
			if(!iss)
				return -2;

		}
	}

	if(reservarMemoria()==-3)
		return -3;

	int i=0;
	while ( myFile.good()) {
		getline(myFile,linea);   // Leer una línea
		if(!linea.empty()){
			istringstream iss(linea);
			int j=0;
			do{
				float value;
				iss >> value;
				if(iss)
					data.d[i][j++] = value;
			}while(iss);
		}
		i++;
	}

	myFile.close();


	return 1;
}


float TSP::evaluarSolucion(Solucion *solucion) {
	SolucionTSP *sol = dynamic_cast<SolucionTSP*>(solucion);
	float distanciaTotal=0;
	// Cálculo de la distancia de cada nodo al siguiente (salvo para el último)
	for(int i=0; i<data.m-1; i++){
		distanciaTotal += data.d[sol->x[i]][sol->x[i+1]];
	}
	// Cálculo de la distancia del último al primero
	distanciaTotal += data.d[sol->x[data.m-1]][0];

	return distanciaTotal;
}

Solucion * TSP::generarSolucionRandom() {
	SolucionTSP *sol = NULL;
	if(data.m!=-1){
		sol = dynamic_cast<SolucionTSP*>(reservarMemoriaSolucion());

		// Elegir m números de forma aleatoria
		int* numerosPorElegir = new int[data.m];
		int* numerosElegidos = new int[data.m];
		// Inicializar la lista de elecciones posibles
		for(int i = 0; i < data.m; i++)
			numerosPorElegir[i] = i;

		for(int i=0; i < data.m; i++)
		{
			int numeroElegido = rand() % (data.m-i);
			// Recordar el numero elegido
			numerosElegidos[i]=numerosPorElegir[numeroElegido];
			// Ponemos en numeros por elegir, el ultimo que era valido, asi
			// son todos validos hasta data.n-i-1
			numerosPorElegir[numeroElegido]=numerosPorElegir[data.m-i-1];
		}

		for(int i=0; i<data.m; i++)
			sol->x[i] = numerosElegidos[i];

		delete [] numerosPorElegir;
		delete [] numerosElegidos;
	}

	return sol;
}

void TSP::imprimirSolucion(Solucion *solucion) {
	SolucionTSP *sol =dynamic_cast<SolucionTSP*>(solucion);
	cout << "***************************" << endl;
	cout << "Viaje a realizar: " << endl;
	for(int i=0; i<data.m; i++){
		cout << sol->x[i] << " ";
	}
	cout << "***************************" << endl;

}

void TSP::imprimirInstancia() {
	cout << "===========================" << endl;
	cout << "Dimensiones:" << endl;
	cout << "m=" << data.m << endl;
	cout << "Matriz de distancias:" << endl;
	for(int i=0; i<data.m; i++){
		for(int j=0; j<data.m; j++){
			cout << data.d[i][j] << " ";
		}
		cout << endl;
	}
	cout << "===========================" << endl;
}

}
