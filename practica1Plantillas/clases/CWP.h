/*
 * MMDP.h
 *
 *  Created on: 06/02/2013
 *      Author: pedroa
 */

#ifndef CWP_H_
#define CWP_H_

#include "Problema.h"

namespace clases {

struct datosInstanciaCWP{
	int n;
	int m;
	bool **c;
};

struct datosSolucionCWP{
	int *x;
};

class CWP: public clases::Problema<struct datosInstanciaCWP,struct datosSolucionCWP> {

public:
	CWP();
	virtual ~CWP();
	int leerInstancia(char *archivo);
	float evaluarSolucion(struct datosSolucionCWP * sol);
	struct datosSolucionCWP * generarSolucionRandom();
	void imprimirSolucion(struct datosSolucionCWP * sol);
	void imprimirInstancia();
	void liberarMemoriaSolucion(struct datosSolucionCWP *sol);
private:
	int reservarMemoria();
	void liberarMemoria();
};

}

#endif /* CWP_H_ */
