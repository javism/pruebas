/*
 * Problema.h
 *
 *  Created on: 06/02/2013
 *      Author: pedroa
 */

#include "Problema.h"

// Manejo de errores
// -1 error al abrir el fichero
// -2 error al leer el formato del fichero
// -3 error en la reserva de memoria
#ifndef PROBLEMATIPADO_H_
#define PROBLEMATIPADO_H_

namespace clases {
// Plantilla de clase
// Establece la estructura de las clases que representarán a los problemas (MMDP, CWP y CPH)
// Estas clases deberán heredar de Problema y definir los métodos virtuales necesarios
// Cada una de estas clases deberá definir dos estructuras, una para los datos de la instancia
// y otra para los datos de la solución. Estas estructuras son las que aparecen como argumentos
// en la plantilla.
template<class SOLUCION>
class ProblemaTipado:public clases::Problema {
public:
	virtual float evaluarSolucion(SOLUCION * sol)=0;
	virtual SOLUCION * generarSolucionRandom()=0;
	virtual void imprimirSolucion(SOLUCION * sol)=0;
	virtual void liberarMemoriaSolucion(SOLUCION *sol)=0;
protected:
	virtual int reservarMemoria()=0;
	virtual void liberarMemoria()=0;
};

}

#endif /* PROBLEMATIPADO_H_ */
