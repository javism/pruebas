/*
 * MMDP.h
 *
 *  Created on: 06/02/2013
 *      Author: pedroa
 */

#ifndef MMDP_H_
#define MMDP_H_

#include "Problema.h"

namespace clases {

struct datosInstanciaMMDP{
	int n;
	int m;
	float **d;
};


class MMDP: public clases::Problema {

public:
	MMDP();
	virtual ~MMDP();
	int leerInstancia(char *archivo);
	float evaluarSolucion(Solucion * sol);
	Solucion * generarSolucionRandom();
	void imprimirSolucion(Solucion * sol);
	void imprimirInstancia();
	void liberarMemoriaSolucion(Solucion *sol);
private:
	int reservarMemoria();
	void liberarMemoria();
	struct datosInstanciaMMDP data;
};

}

#endif /* MMDP_H_ */
