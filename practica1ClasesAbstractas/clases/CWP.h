/*
 * MMDP.h
 *
 *  Created on: 06/02/2013
 *      Author: pedroa
 */

#ifndef CWP_H_
#define CWP_H_

#include "Problema.h"

namespace clases {

struct datosInstanciaCWP{
	int n;
	int m;
	bool **c;
};

class CWP: public clases::Problema {

public:
	CWP();
	virtual ~CWP();
	int leerInstancia(char *archivo);
	float evaluarSolucion(Solucion * sol);
	Solucion * generarSolucionRandom();
	void imprimirSolucion(Solucion * sol);
	void imprimirInstancia();
	void liberarMemoriaSolucion(Solucion *sol);
private:
	int reservarMemoria();
	void liberarMemoria();
	struct datosInstanciaCWP data;
};

}

#endif /* CWP_H_ */
