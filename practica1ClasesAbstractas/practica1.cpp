//============================================================================
// Name        : practica1.cpp
// Author      : Pedro A. Gutiérrez
// Version     :
// Copyright   : Universidad de Córdoba
// Description : Hello World in C, Ansi-style
//============================================================================

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <iostream>
#include <ctime>    // Para cojer la hora time()
#include <cstdlib>  // Para establecer la semilla srand() y generar números aleatorios rand()
#include <string.h>
#include "clases/MMDP.h"
#include "clases/CWP.h"
#include "clases/CPH.h"
#include "clases/TSP.h"
#include "clases/Problema.h"
#include "clases/Solucion.h"

using namespace clases;
using namespace std;

int procesarDatos(char *nombre, Problema *p) {
	int error=EXIT_SUCCESS;
	if((error=p->leerInstancia(nombre))!=1){
		cout << "Error al leer la instancia del problema" << endl;
		return error;
	}

	p->imprimirInstancia();
	float maximo = 0;
	float minimo = 0;
	for(int i=0; i<1000; i++){
		Solucion *x = p->generarSolucionRandom();
		cout << "Solución generada (iteracion " << i << "): " << endl;
		p->imprimirSolucion(x);
		float valorObtenido = p->evaluarSolucion(x);
		if(i==0){
			maximo = minimo = valorObtenido;
		}
		else{
			if(valorObtenido>maximo)
				maximo = valorObtenido;
			if(valorObtenido<minimo)
				minimo = valorObtenido;
		}
		cout << "Función objetivo: " << valorObtenido << endl;
		p->liberarMemoriaSolucion(x);
	}
	cout << endl << endl << "==> Valores extremos generados: máximo=" << maximo << " mínimo=" << minimo << endl;

	delete p;
	return error;
}


int main(int argc, char **argv) {

	bool sflag = 0, fflag = 0, pflag = 0;
	char *svalue = NULL, *fvalue = NULL , *pvalue = NULL;
    int c;

    opterr = 0;

    // a: opción que requiere un argumento
    // a:: el argumento requerido es opcional
    while ((c = getopt(argc, argv, "s:f:p:")) != -1)
    {
    	switch(c){
    	case 's':
    		sflag = true;
    		svalue = optarg;
    		break;
    	case 'f':
    		fflag = true;
    		fvalue = optarg;
    		break;
    	case 'p':
    		pflag = true;
    		pvalue = optarg;
    		break;
        case '?':
            if (optopt == 's' || optopt == 'f' || optopt == 'p')
                fprintf (stderr, "La opción -%c requiere un argumento.\n", optopt);
            else if (isprint (optopt))
                fprintf (stderr, "Opción desconocida `-%c'.\n", optopt);
            else
                fprintf (stderr,
                         "Caracter de opción desconocido `\\x%x'.\n",
                         optopt);
            return EXIT_FAILURE;
        default:
            return EXIT_FAILURE;
    	}
    }

    // Nombre del fichero
    if(!fflag){
    	cout<< "Es necesario especificar el nombre del fichero a procesar" << endl;
    	return -1;
    }

    // Semilla de los números aleatorios
    if(sflag){
    	cout<< "Semilla " << svalue << ". ";
    	srand(atol(svalue));
    }
    else{
		long semilla = time(0);
		srand(semilla);
    	cout<< "Semilla " << semilla << ". ";
    }


    // Tipo de problema
    Problema *p;
	if(pflag && strcmp(pvalue,"MMDP") == 0){
		cout<< "Problema MMDP. Instancia " << fvalue << "..."<< endl;
		p = new MMDP();
	}
	else if(pflag && strcmp(pvalue,"CWP") == 0){
		cout<< "Problema CWP. Instancia " << fvalue << "..."<< endl;
		p = new CWP();
	}
	else if(pflag && strcmp(pvalue,"CPH") == 0){
		cout<< "Problema CPH. Instancia " << fvalue << "..."<< endl;
		p = new CPH();
	}
	else if(pflag && strcmp(pvalue,"TSP") == 0){
		cout<< "Problema TSP. Instancia " << fvalue << "..."<< endl;
		p = new TSP();
	}
	// Por defecto, el MMDP
	else{
		cout<< "Problema MMDP. Instancia " << fvalue << "..."<< endl;
		p = new MMDP();
	}
	if(procesarDatos(fvalue,p) != 1)
		return EXIT_FAILURE;
	else
		return EXIT_SUCCESS;
}

