/*
 * Problema.h
 *
 *  Created on: 06/02/2013
 *      Author: pedroa
 */

// Manejo de errores
// -1 error al abrir el fichero
// -2 error al leer el formato del fichero
// -3 error en la reserva de memoria
#ifndef SOLUCIONCPH_H_
#define SOLUCIONCPH_H_

namespace clases {

class SolucionCPH : public Solucion {
public:
	int *x;
};

}

#endif /* SOLUCIONCPH_H_ */
