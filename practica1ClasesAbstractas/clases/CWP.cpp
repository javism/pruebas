/*
 * MMDP.cpp
 *
 *  Created on: 06/02/2013
 *      Author: pedroa
 */

#include "CWP.h"
#include "SolucionCWP.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdlib>  // Para establecer la semilla srand() y generar números aleatorios rand()
#include <limits>

using namespace std;

namespace clases {

CWP::CWP() {
	data.n=-1;
	data.m=-1;
}

CWP::~CWP() {
	if(data.c!=0)
		liberarMemoria();
}

int CWP::reservarMemoria() {
	if(data.n!=-1){
		data.c = new bool*[data.n];
		if(data.c==0)
			return -1;
		for(int i=0; i<data.n; i++){
			data.c[i] = new bool[data.n];
			if(data.c[i]==0)
				return -1;
		}

		for(int i=0; i<data.n; i++)
			for(int j=0; j<data.n; j++)
				data.c[i][j] = false;
		return 1;
	}
	else
		return -1;
}

void CWP::liberarMemoria() {
	for(int i=0; i<data.n; i++)
		delete[] data.c[i];
	delete[] data.c;
}

void CWP::liberarMemoriaSolucion(Solucion *solucion){
	SolucionCWP *sol =dynamic_cast<SolucionCWP*>(solucion);
	delete[] sol->x;
}

int CWP::leerInstancia(char *archivo) {
	ifstream myFile(archivo);    // Crear un stream de entrada

    if (!myFile.is_open()) {
       cout << "ERROR: no puedo abrir el fichero " << archivo << endl;
       return -1;
    }

	string linea;
	if( myFile.good()) {
		getline(myFile,linea);   // Leer una línea
		istringstream iss(linea);
		iss >> data.n;
		iss >> data.n;
		iss >> data.m;
	}

	if(reservarMemoria()==-3)
		return -3;

	while ( myFile.good()) {
		getline(myFile,linea);   // Leer una línea
		if (! linea.empty()) {
			istringstream iss(linea);
			int iIndex;
			iss >> iIndex;
			if(!iss)
				return -2;
			int jIndex;
			iss >> jIndex;
			if(!iss)
				return -2;
			data.c[iIndex-1][jIndex-1] = data.c[jIndex-1][iIndex-1] = true;
		}
	}

	myFile.close();

	return 1;
}


float CWP::evaluarSolucion(Solucion *solucion) {
	SolucionCWP *sol =dynamic_cast<SolucionCWP*>(solucion);
	int maximoCorte = 0;
	// Minima Distancia
	for(int i=0; i<(data.n-1); i++){
		int corte=0;
		//cout << "HOLA" << i;
		for(int origen=0; origen<=i; origen++){
			for(int destino=(i+1); destino<data.n; destino++){
				//cout << "Valorando " << x->x[origen] << " y " << x->x[destino] <<  data.c[indicesOrdenados[origen] - 1 ][indicesOrdenados[destino] - 1] << endl;
				if(data.c[sol->x[origen]][sol->x[destino]])
					corte++;
			}
		}
		//cout << corte << endl;
		if(corte>maximoCorte)
			maximoCorte = corte;
	}

	return maximoCorte;
}

struct Solucion * CWP::generarSolucionRandom() {
	SolucionCWP *x = NULL;
	if(data.n!=-1){
		x = new SolucionCWP;
		x->x = new int[data.n];
		for(int i=0; i<data.n; i++)
			x->x[i] = 0;

		int* numerosPorElegir = new int[data.n];
		int* numerosElegidos = new int[data.n];
		// Inicializar la lista de elecciones posibles
		for(int i = 0; i < data.n; i++)
			numerosPorElegir[i] = i;

		for(int i=0; i < data.n; i++)
		{
			int numeroElegido = rand() % (data.n-i);
			// Recordar el numero elegido
			numerosElegidos[i]=numerosPorElegir[numeroElegido];
			// Ponemos en numeros por elegir, el ultimo que era valido, asi
			// son todos validos hasta data.n-i-1
			numerosPorElegir[numeroElegido]=numerosPorElegir[data.n-i-1];
		}

		for(int i=0; i<data.n; i++)
			x->x[i] = numerosElegidos[i];
		delete [] numerosPorElegir;
		delete [] numerosElegidos;
	}

	return x;
}

void CWP::imprimirSolucion(Solucion *solucion) {
	SolucionCWP *sol =dynamic_cast<SolucionCWP*>(solucion);
	cout << "***************************" << endl;
	cout << "Ordenación realizada: " << endl;
	for(int i=0; i<data.n; i++){
		cout << sol->x[i] << " ";
	}
	cout << endl;
	cout << "***************************" << endl;
}

void CWP::imprimirInstancia() {
	cout << "===========================" << endl;
	cout << "Dimensiones:" << endl;
	cout << "n=" << data.n << ", m=" << data.m << endl;
	cout << "Matriz de conectividad:" << endl;
	for(int i=0; i<data.n; i++){
		for(int j=0; j<data.n; j++){
			cout << data.c[i][j] << " ";
		}
		cout << endl;
	}
	cout << "===========================" << endl;
}

}
