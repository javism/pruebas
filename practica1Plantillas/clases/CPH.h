/*
 * MMDP.h
 *
 *  Created on: 06/02/2013
 *      Author: pedroa
 */

#ifndef CPH_H_
#define CPH_H_

#include "Problema.h"

namespace clases {

struct datosInstanciaCPH{
	int n;
	int p;
	int c;
	float **d;
	float *f;
};

struct datosSolucionCPH{
	int *x;
};

class CPH: public clases::Problema<struct datosInstanciaCPH,struct datosSolucionCPH> {

public:
	CPH();
	virtual ~CPH();
	int leerInstancia(char *archivo);
	float evaluarSolucion(struct datosSolucionCPH * sol);
	struct datosSolucionCPH * generarSolucionRandom();
	void imprimirSolucion(struct datosSolucionCPH * sol);
	void imprimirInstancia();
	void liberarMemoriaSolucion(struct datosSolucionCPH *sol);
	struct datosSolucionCPH * reservarMemoriaSolucion();
private:
	int reservarMemoria();
	void liberarMemoria();
};

}

#endif /* CPH_H_ */
