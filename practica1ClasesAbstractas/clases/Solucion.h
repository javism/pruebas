/*
 * Problema.h
 *
 *  Created on: 06/02/2013
 *      Author: pedroa
 */

// Manejo de errores
// -1 error al abrir el fichero
// -2 error al leer el formato del fichero
// -3 error en la reserva de memoria
#ifndef SOLUCION_H_
#define SOLUCION_H_

namespace clases {

class Solucion {
public:
	Solucion(){};
	virtual ~Solucion(){};
};

}

#endif /* PROBLEMA_H_ */
