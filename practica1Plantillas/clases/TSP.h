/*
 * MMDP.h
 *
 *  Created on: 06/02/2013
 *      Author: pedroa
 */

#ifndef TSP_H_
#define TSP_H_

#include "Problema.h"

namespace clases {

struct datosInstanciaTSP{
	int m;
	float **d;
};

struct datosSolucionTSP{
	int *x;
};

class TSP: public clases::Problema<struct datosInstanciaTSP,struct datosSolucionTSP> {

public:
	TSP();
	virtual ~TSP();
	int leerInstancia(char *archivo);
	float evaluarSolucion(struct datosSolucionTSP * sol);
	struct datosSolucionTSP * generarSolucionRandom();
	void imprimirSolucion(struct datosSolucionTSP * sol);
	void imprimirInstancia();
	void liberarMemoriaSolucion(struct datosSolucionTSP *sol);
	struct datosSolucionTSP * reservarMemoriaSolucion();
private:
	int reservarMemoria();
	void liberarMemoria();
};

}

#endif /* TSP_H_ */
