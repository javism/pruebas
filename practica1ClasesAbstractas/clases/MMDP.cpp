/*
 * MMDP.cpp
 *
 *  Created on: 06/02/2013
 *      Author: pedroa
 */

#include "MMDP.h"
#include "SolucionMMDP.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdlib>  // Para establecer la semilla srand() y generar números aleatorios rand()
#include <limits>

using namespace std;

namespace clases {

MMDP::MMDP() {
	data.n=-1;
	data.m=-1;
}

MMDP::~MMDP() {
	if(data.d!=0)
		liberarMemoria();
}

int MMDP::reservarMemoria() {
	if(data.n!=-1){
		data.d = new float*[data.n];
		if(data.d==0)
			return -3;
		for(int i=0; i<data.n; i++){
			data.d[i] = new float[data.n];
			if(data.d[i]==0)
				return -3;
		}

		for(int i=0; i<data.n; i++)
			for(int j=0; j<data.n; j++)
				data.d[i][j] = 0;
		return 1;
	}
	else
		return -3;
}

void MMDP::liberarMemoria() {
	for(int i=0; i<data.n; i++)
		delete[] data.d[i];
	delete[] data.d;
}

void MMDP::liberarMemoriaSolucion(Solucion *solucion){
	SolucionMMDP *sol = dynamic_cast<SolucionMMDP*>(solucion);
	delete[] sol->x;
	delete sol;
}

int MMDP::leerInstancia(char *archivo) {
	ifstream myFile(archivo);    // Crear un stream de entrada

    if (!myFile.is_open()) {
       cout << "ERROR: no puedo abrir el fichero " << archivo << endl;
       return -1;
    }

	string linea;
	if( myFile.good()) {
		getline(myFile,linea);   // Leer una línea
		istringstream iss(linea);
		iss >> data.n;
		if(!iss)
			return -2;
		iss >> data.m;
		if(!iss)
			return -2;
	}

	if(reservarMemoria()==-3)
		return -3;

	while ( myFile.good()) {
		getline(myFile,linea);   // Leer una línea
		if(!linea.empty()){
			istringstream iss(linea);
			int iIndex;
			iss >> iIndex;
			if(!iss)
				return -2;
			int jIndex;
			iss >> jIndex;
			if(!iss)
				return -2;
			iss >> data.d[iIndex][jIndex];
			if(!iss)
				return -2;
			data.d[jIndex][iIndex] = data.d[iIndex][jIndex];
		}
	}

	myFile.close();

	return 1;
}


float MMDP::evaluarSolucion(Solucion *solucion) {
	SolucionMMDP *sol =dynamic_cast<SolucionMMDP*>(solucion);
	float minimaDistancia=numeric_limits<float>::max();
	int numElementos = 0;
	for(int i=0; i<data.n; i++){
		if(sol->x[i])
			numElementos++;
	}
	if(numElementos == data.m){
		// Minima Distancia
		for(int i=0; i<data.n; i++){
			for(int j=(i+1); j<data.n; j++){
				if(sol->x[i] && sol->x[j]){
					if(data.d[i][j] < minimaDistancia)
						minimaDistancia = data.d[i][j];
				}
			}
		}

	}
	return minimaDistancia;
}

Solucion * MMDP::generarSolucionRandom() {
	SolucionMMDP *sol = NULL;
	if(data.n!=-1){
		sol = new SolucionMMDP;
		if(sol == 0)
			return NULL;
		sol->x = new bool[data.n];
		for(int i=0; i<data.n; i++)
			sol->x[i] = 0;

		int* numerosPorElegir = new int[data.n];
		int* numerosElegidos = new int[data.m];

		// Problemas con la memoria
		if(numerosPorElegir==0 || numerosElegidos==0 || sol->x == 0){
			return NULL;
		}

		// Inicializar la lista de elecciones posibles
		for(int i = 0; i < data.n; i++)
			numerosPorElegir[i] = i;

		for(int i=0; i < data.m; i++)
		{
			int numeroElegido = rand() % (data.n-i);
			// Recordar el numero elegido
			numerosElegidos[i]=numerosPorElegir[numeroElegido];
			// Ponemos en numeros por elegir, el ultimo que era valido, asi
			// son todos validos hasta data.n-i-1
			numerosPorElegir[numeroElegido]=numerosPorElegir[data.n-i-1];
		}

		for(int i=0; i<data.m; i++)
			sol->x[numerosElegidos[i]] = 1;
		delete [] numerosPorElegir;
		delete [] numerosElegidos;
	}

	return sol;
}

void MMDP::imprimirSolucion(Solucion *solucion) {
	SolucionMMDP *sol =dynamic_cast<SolucionMMDP*>(solucion);
	cout << "***************************" << endl;
	cout << "Elementos escogidos: " << endl;
	for(int i=0; i<data.n; i++){
		cout << sol->x[i] << " ";
	}
	cout << endl;
	cout << "***************************" << endl;
}

void MMDP::imprimirInstancia() {
	cout << "===========================" << endl;
	cout << "Dimensiones:" << endl;
	cout << "m=" << data.m << ", n=" << data.n << endl;
	cout << "Matriz de conectividad:" << endl;
	for(int i=0; i<data.n; i++){
		for(int j=0; j<data.n; j++){
			cout << data.d[i][j] << " ";
		}
		cout << endl;
	}
	cout << "===========================" << endl;
}

}
