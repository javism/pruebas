/*
 * MMDP.h
 *
 *  Created on: 06/02/2013
 *      Author: pedroa
 */

#ifndef TSP_H_
#define TSP_H_

#include "Problema.h"

namespace clases {

struct datosInstanciaTSP{
	int m;
	float **d;
};

class TSP: public clases::Problema{

public:
	TSP();
	virtual ~TSP();
	int leerInstancia(char *archivo);
	float evaluarSolucion(Solucion * sol);
	Solucion * generarSolucionRandom();
	void imprimirSolucion(Solucion * sol);
	void imprimirInstancia();
	void liberarMemoriaSolucion(Solucion *sol);
	Solucion * reservarMemoriaSolucion();
private:
	int reservarMemoria();
	void liberarMemoria();
	struct datosInstanciaTSP data;
};

}

#endif /* TSP_H_ */
