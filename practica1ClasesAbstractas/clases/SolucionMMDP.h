/*
 * Problema.h
 *
 *  Created on: 06/02/2013
 *      Author: pedroa
 */

// Manejo de errores
// -1 error al abrir el fichero
// -2 error al leer el formato del fichero
// -3 error en la reserva de memoria
#ifndef SOLUCIONMMDP_H_
#define SOLUCIONMMDP_H_

namespace clases {

class SolucionMMDP : public Solucion {
public:
	bool *x;
};

}

#endif /* SOLUCIONMMDP_H_ */
