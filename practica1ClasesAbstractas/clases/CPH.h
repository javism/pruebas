/*
 * MMDP.h
 *
 *  Created on: 06/02/2013
 *      Author: pedroa
 */

#ifndef CPH_H_
#define CPH_H_

#include "Problema.h"

namespace clases {

struct datosInstanciaCPH{
	int n;
	int p;
	int c;
	float **d;
	float *f;
};

class CPH: public clases::Problema {

public:
	CPH();
	virtual ~CPH();
	int leerInstancia(char *archivo);
	float evaluarSolucion(Solucion * sol);
	Solucion * generarSolucionRandom();
	void imprimirSolucion(Solucion * sol);
	void imprimirInstancia();
	void liberarMemoriaSolucion(Solucion *sol);
	Solucion * reservarMemoriaSolucion();
private:
	int reservarMemoria();
	void liberarMemoria();
	datosInstanciaCPH data;
};

}

#endif /* CPH_H_ */
